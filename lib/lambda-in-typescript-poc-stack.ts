import * as cdk from 'aws-cdk-lib';
import {Construct} from 'constructs';
import {Vpc} from 'aws-cdk-lib/aws-ec2';
import {HelloWorld} from "./hello-world";
import {ApplicationTargetGroup, CfnListenerRule, TargetType} from 'aws-cdk-lib/aws-elasticloadbalancingv2';
import {LambdaTarget} from "aws-cdk-lib/aws-elasticloadbalancingv2-targets";

const ALB_LISTENER_ARN = `arn of the alb`;

export class LambdaInTypescriptPocStack extends cdk.Stack {
    constructor(scope: Construct, id: string, props?: cdk.StackProps) {
        super(scope, id, props);

        const helloWorldConstruct = new HelloWorld(this, 'hello-world');

        const vpc = Vpc.fromLookup(this, 'Vpc', {
            isDefault: false
        });

        const applicationTargetGroup = new ApplicationTargetGroup(this, 'lambda-target-group', {
            vpc,
            targetGroupName: 'lambda-target-group',
            targetType: TargetType.LAMBDA,
            targets: [new LambdaTarget(helloWorldConstruct.helloFunction)]
        });

        new CfnListenerRule(this, 'lambda-listener-rule', {
            priority: 1,
            listenerArn: ALB_LISTENER_ARN,
            conditions: [{
                field: 'path-pattern',
                values: ['/lambda/*']
            }],
            actions: [{
                type: 'forward',
                targetGroupArn: applicationTargetGroup.targetGroupArn
            }]
        });

    }
}
