import axios, {AxiosRequestConfig} from 'axios';

const SUCCESS = {
    statusCode: 200,
    multiValueHeaders: {
        'Content-Type': ['text/html; charset=utf-8']
    },
    body: JSON.stringify({
        message: 'hellow world'
    })
};

const ERROR = {
    statusCode: 500,
    multiValueHeaders: {
        'Content-Type': ['text/html; charset=utf-8']
    },
    body: JSON.stringify({
        message: 'exception occured'
    })
};

export class HttpGet {
    constructor(private url: string) {
    }

    config(): AxiosRequestConfig {
        return {
            timeout: 2000
        } as AxiosRequestConfig
    }

    async get(config: AxiosRequestConfig) {
        return await axios.get(this.url);
    }
}

export const handler = async (event: any, context: any) => {
    console.log(`Event: ${JSON.stringify(event, null, 2)}`);
    console.log(`Context: ${JSON.stringify(context, null, 2)}`);
    try {
        const httpGet = new HttpGet('http://something.that.does.not.exist');
        const config = httpGet.config();
        const request = await httpGet.get(config);
        return SUCCESS;
    } catch (error) {
        console.log('ERROR---------------------->', error);
        return ERROR;
    }
}
