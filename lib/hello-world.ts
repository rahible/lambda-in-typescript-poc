import {Construct} from 'constructs';
import {NodejsFunction} from "aws-cdk-lib/aws-lambda-nodejs";

export class HelloWorld extends Construct {
    helloFunction: NodejsFunction;

    constructor(scope: Construct, id: string) {
        super(scope, id);
        // the id 'function' matches the 'function' part of the hello-world.function.ts file
        this.helloFunction = new NodejsFunction(this, 'function');
    }

}
