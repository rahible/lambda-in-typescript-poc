# Example for deploying a TypeScript Lambda

## Pre-Requisites

1) Vpc is created and exists.
2) ALB has been created.
3) CDK has been configured in the environment.

## Changes

* Update ALB_LISTENER_ARN in lambda-in-typescript-poc-stack.ts with the ALB Listener ARN.

## Test

* Use the lambda console to test the execution.
